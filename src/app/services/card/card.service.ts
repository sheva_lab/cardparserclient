import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {APIUtil} from '../../utils/APIUtil';

@Injectable({
  providedIn: 'root'
})
export class CardService {
    constructor(private httpClient: HttpClient) { }

    getCards(formData: FormData) {
        return this.httpClient.post<any>(APIUtil.CARDS, formData);
    }
}
