export class ArrayUtil {
    static groupBy = key => array =>
        array.reduce(
            (objectsByKeyValue, obj) => ({
                ...objectsByKeyValue,
                [obj[key]]: (objectsByKeyValue[obj[key]] || []).concat(obj)
            }),
            {}
        )
}
