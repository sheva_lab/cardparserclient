import {Component, OnInit} from '@angular/core';
import {CardService} from '../../services/card/card.service';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ChartDataSets, ChartOptions, ChartType} from 'chart.js';
import {Label} from 'ng2-charts';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';

import {ToastrService} from 'ngx-toastr';
import {ArrayUtil} from '../../utils/ArrayUtil';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

    uploadForm: FormGroup;
    title = 'card parser';
    cardsData = [];
    errors: 0;

    barChartPlugins = [pluginDataLabels];
    barChartLabels: Label[] = [];
    barChartType: ChartType = 'bar';
    barChartLegend = true;
    barChartOptions: ChartOptions = {
        responsive: true,
        scales: {
            xAxes: [ {
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Date'
                }
            } ],
            yAxes: [ {
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Sales count'
                }
            } ]
        },
        plugins: {
            datalabels: {
                anchor: 'end',
                align: 'end',
                font: {
                    size: 10,
                }
            }
        }
    };

    public barChartData: ChartDataSets[] = [];

    constructor(private formBuilder: FormBuilder, private toastr: ToastrService, private cardService: CardService) {
    }

    ngOnInit() {
        this.uploadForm = this.formBuilder.group({
            file: ['']
        });
    }

    onFileSelect(event) {
        if (event.target.files.length > 0) {
            const file = event.target.files[0];
            this.uploadForm.get('file').setValue(file);
        }
    }

    onSubmit() {
        const formData = new FormData();
        formData.append('file', this.uploadForm.get('file').value);
        this.cardService.getCards(formData).subscribe(
            res => {
                const {cards, errors} = res;
                this.barChartLabels = cards.map(({date}) => date).reduce((uniq, item) => uniq.includes(item) ? uniq : [...uniq, item], []);
                this.barChartData = [];
                this.cardsData = cards;

                const groupByCardType = ArrayUtil.groupBy('cardType');

                const groupedCards = groupByCardType(cards.map(card => {
                    const {cardType} = card;
                    return {
                        ...card,
                        cardType: cardType.toUpperCase()
                    };
                }));

                const populateBarChartData = (cards: any) => {
                    const dataArr = [];
                    for (let i = 0; i < this.barChartLabels.length; i++) {
                        const label = this.barChartLabels[i];
                        const cardObj = cards.filter(({date}) => date === label)[0];
                        const salesCount = cardObj ? cardObj.salesCount : 0;
                        dataArr.push(salesCount);
                    }

                    return dataArr;
                };

                for (const [key, value] of Object.entries(groupedCards)) {
                    this.barChartData.push({
                        data: populateBarChartData(value),
                        label: key
                    });
                }

                this.errors = errors;

                this.toastr.success('File successfully uploaded!');

            },
            err => {
                console.error(err);
                this.toastr.error(err);
            }
        );
    }
}
